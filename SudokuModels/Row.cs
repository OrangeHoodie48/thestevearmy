﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheSteveArmy.SudokuModels
{
    public class Row
    {
        public Row()
        {
            Cells = new List<Cell>(); 
            for(int i = 0; i < 9; i++)
            {
                Cells.Add(new Cell());
            }
        }
       public List<Cell> Cells { get; set; }
    }
}
