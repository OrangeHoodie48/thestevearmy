﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;
using TheSteveArmy.Services;
using TheSteveArmy.ViewModels;

namespace TheSteveArmy.Controllers
{
    public class HomeController : Controller
    {
        IDataService dataService;
        UserManager<IdentityModel> userManager; 
        public HomeController(IDataService dataService, UserManager<IdentityModel> userManager)
        {
            this.dataService = dataService;
            this.userManager = userManager; 
        }
        public IActionResult Index()
        {
            IndexViewModel model = new IndexViewModel();
            model.BlogPosts = new List<BlogPostWrapper>();
            foreach(BlogPost post in dataService.GetCurrentBlogPosts())
            {
                BlogPostWrapper wrap = new BlogPostWrapper();
                wrap.LoadFromBlogPost(post);
                wrap.Comments = dataService.GetBlogPostComments(post.Id);
                model.BlogPosts.Add(wrap);
            }
            return View(model);
        }     

        [Authorize]
        [HttpPost]
        public IActionResult NewBlogComment(IndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index");
            }
            Comment comment = new Comment();
            comment.CreatedAt = DateTime.Now;
            comment.PageId = model.BlogPostId;
            comment.PageType = "Blog";
            comment.Text = model.NewComment;
            comment.UserName = model.NewCommentUserName;
            dataService.AddComment(comment);
            return RedirectToAction("Index", "Home");
        }

        public IActionResult RemoveComment(int id)
        {
            dataService.RemoveComment(id);
            return RedirectToAction("Index", "Home");
        }
    }
}
