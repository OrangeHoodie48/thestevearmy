﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;
using TheSteveArmy.Services;
using TheSteveArmy.ViewModels;

namespace TheSteveArmy.Controllers
{
    public class ProjectController : Controller
    {
        IDataService dataService;
        public ProjectController(IDataService dataService)
        {
            this.dataService = dataService;
        }

        [HttpGet]
        public IActionResult NewProject()
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            ProjectViewModel model = new ProjectViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewProject(ProjectViewModel model)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            Project proj = new Project();
            await model.LoadProject(proj);
            dataService.AddProject(proj);
            return RedirectToAction("Index", "Home");
        }
        public IActionResult AllProjects()
        {
            List<Project> projects = dataService.GetProjects();
            return View(projects);
        }

        public IActionResult AllProjectsEdit()
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<Project> projects = dataService.GetProjects();
            return View(projects);
        }

        public IActionResult RemoveProject(int id)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            dataService.RemoveProject(id);
            return RedirectToAction("AllProjectsEdit", "Project");
        }

        public IActionResult ProjectPage(int id)
        {
            Project proj = dataService.GetProject(id);
            ProjectPageViewModel model = new ProjectPageViewModel();
            model.LoadFromProject(proj);
            model.Comments = dataService.GetProjectComments(proj.Id);
            return View(model);
        }

        [HttpGet]
        public IActionResult EditProject(int id)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            Project proj = dataService.GetProject(id);
            EditProjectViewModel model = new EditProjectViewModel();
            model.LoadFromProject(proj);
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> EditProject(EditProjectViewModel model)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            Project proj = dataService.GetProject(model.Id);
            await model.LoadProject(proj);
            dataService.UpdateProject(proj);
            return RedirectToAction("AllProjectsEdit", "Project");
        }

        [Authorize]
        public IActionResult NewProjectComment(ProjectPageViewModel model)
        {
            Comment comment = new Comment();
            comment.PageId = model.Id;
            comment.PageType = "Project";
            comment.UserName = model.NewCommentUserName;
            comment.Text = model.NewComment;
            comment.CreatedAt = DateTime.Now;
            dataService.AddComment(comment); 
            return RedirectToAction("ProjectPage", "Project", new { id = model.Id });
        }

        public IActionResult RemoveComment(int id)
        {
            Comment c = dataService.GetComment(id);
            int pageId = c.PageId; 
            dataService.RemoveComment(id);
            return RedirectToAction("ProjectPage", "Project", new { id = pageId });
        }

    }
}
