﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;
using TheSteveArmy.Services;
using TheSteveArmy.ViewModels;

namespace TheSteveArmy.Controllers
{

    public class BlogPostController : Controller
    {
        IDataService dataService; 
        public BlogPostController(IDataService dataService)
        {
            this.dataService = dataService;
        }

        public IActionResult CurrentBlogPosts()
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<BlogPost> blogPosts = dataService.GetCurrentBlogPosts();
            return View(blogPosts);
        }

        [HttpGet]
        public IActionResult NewBlogPost()
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            BlogPostViewModel model = new BlogPostViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> NewBlogPost(BlogPostViewModel model)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            model.CreatedAt = DateTime.Now;

            BlogPost post = new BlogPost();
            await model.LoadBlogPost(post);

            dataService.AddBlogPost(post);
           
            return RedirectToAction("Index", "Home");
        }

        public IActionResult RemoveBlogPost(int id)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }

            BlogPost post = dataService.GetBlogPost(id); 
            if(post != null)
            {
                post.Removed = true;
                dataService.UpdateBlogPost(post);
            }
            return RedirectToAction("CurrentBlogPosts", "BlogPost");
        }

        [HttpGet]
        public IActionResult EditBlogPost(int id)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }

            BlogPost post = dataService.GetBlogPost(id);
            BlogPostViewModel model = new BlogPostViewModel();
            model.LoadFromBlogPost(post);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditBlogPost(BlogPostViewModel model)
        {
            if (!User.Identity.Name.Equals("KalnomosTheBold"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            BlogPost realPost = dataService.GetBlogPost(model.Id);
            await model.EditBlogPost(realPost);
            dataService.UpdateBlogPost(realPost);
            return RedirectToAction("CurrentBlogPosts", "BlogPost");
        }
        
    }
}
