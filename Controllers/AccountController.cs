﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using TheSteveArmy.Models;
using TheSteveArmy.Services;
using TheSteveArmy.ViewModels;

namespace TheSteveArmy.Controllers
{
    public class AccountController : Controller
    {
        SignInManager<IdentityModel> signInManager;
        IDataService dataService;
        UserManager<IdentityModel> userManager;
        IHostingEnvironment env; 
        public AccountController(SignInManager<IdentityModel> signInManager, IDataService dataService, UserManager<IdentityModel> userManager, IHostingEnvironment env)
        {
            this.signInManager = signInManager;
            this.dataService = dataService;
            this.userManager = userManager;
            this.env = env;
        }
        
        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            LoginViewModel model = new LoginViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            var result = await signInManager.PasswordSignInAsync(model.Username, model.Password, true, false);
            if(result.Succeeded == true)
            {
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("", "User Name Or Password Incorrect.");
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> LogOut()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public IActionResult CreateTempUser()
        {
            TempUser temp = new TempUser();
            return View(temp);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateTempUser(TempUser user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }

            if (dataService.TempUserTaken(user.UserName, user.Password))
            {
                ModelState.AddModelError("", "Username or password taken in temp");
                return View(user);
            }
            IdentityModel model = new IdentityModel();
            model.UserName = user.UserName;
            model.Email = user.Email;
            var result = await userManager.CreateAsync(model, user.Password);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "Username or password taken");
                return View(user);
            }
            await userManager.DeleteAsync(model);


            var smtpClient = new SmtpClient
            {
                Host = "smtp.aol.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("stevend39addario@aol.com", "C1c3r0Th3Th1rd")
            };
            using (var message = new MailMessage("stevend39addario@aol.com", user.Email) { Subject = "Account Registration", Body = user.BuildEmail() })
            {
                smtpClient.Send(message);
            }
            dataService.AddTempUser(user);
            return RedirectToAction("TempCreateSuccess", "Account");
        }

        public IActionResult TempCreateSuccess()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CompleteRegistration()
        {
            TempUser user = new TempUser();
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CompleteRegistration(TempUser user)
        {
            if (!ModelState.IsValid) {
                return View(user);
            }
            if (!dataService.TempUserGoodForCompletingRegistration(user))
            {
                ModelState.AddModelError("", "Incorrect Data");
                return View();
            }
            IdentityModel model = new IdentityModel();
            model.UserName = user.UserName;
            model.Email = user.Email;
            await userManager.CreateAsync(model, user.Password);
            await signInManager.PasswordSignInAsync(model.UserName, user.Password, true, false);
            dataService.RemoveTempUserWithoutId(user);
            return RedirectToAction("Index", "Home");
        }
        
    }
}
