﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;

namespace TheSteveArmy.ViewModels
{
    public class ProjectViewModel
    {
        public int Id { get; set; }
        [Required]
        public string Heading { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public virtual IFormFile Image { get; set; }
        [Required]
        public ProjectCategoriesEnum Category { get; set; }
        [Required]
        public string BriefDescription { get; set; }
        public string Repository { get; set; }
        public string AdditionalLinks { get; set; }

        public async Task<Project> LoadProject(Project proj)
        {
            proj.Heading = Heading;
            proj.Body = Body;
            proj.Repository = Repository;
            proj.AdditionalLinks = AdditionalLinks;
            proj.Category = Category;
            proj.BriefDescription = BriefDescription;
            if (Image != null)
            {
                using (var mem = new MemoryStream())
                {
                    await Image.CopyToAsync(mem);
                    proj.Image = mem.ToArray();
                }
            }
            return proj;

        }

    }


}
