﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;

namespace TheSteveArmy.ViewModels
{
    public class IndexViewModel
    {
        public List<BlogPostWrapper> BlogPosts { get; set; }
        public int BlogPostId { get; set; }
        public string NewComment { get; set; }
        public string NewCommentUserName { get; set; }
    }
}
