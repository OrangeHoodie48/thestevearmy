﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;

namespace TheSteveArmy.ViewModels
{
    public class ProjectPageViewModel
    {
        public int Id { get; set; }
        public string Heading { get; set; }
        public string Body { get; set; }
        public byte[] Image { get; set; }
        public ProjectCategoriesEnum Category { get; set; }
        public string BriefDescription { get; set; }
        public string Repository { get; set; }
        public string AdditionalLinks { get; set; }
        public List<Comment> Comments { get; set; }
        public string NewComment { get; set; }
        public string NewCommentUserName { get; set; }
        public void LoadFromProject(Project proj)
        {
            Id = proj.Id;
            Heading = proj.Heading;
            Body = proj.Body;
            Image = proj.Image;
            Category = proj.Category;
            BriefDescription = proj.BriefDescription;
            Repository = proj.Repository;
            AdditionalLinks = proj.AdditionalLinks;
        }

    }
}
