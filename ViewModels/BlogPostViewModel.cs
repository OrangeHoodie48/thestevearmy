﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;

namespace TheSteveArmy.ViewModels
{
    public class BlogPostViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        [Required]
        public string PostHeader { get; set; }
        [Required]
        public string PostBody { get; set; }

        public IFormFile PostImage { get; set; }

        public bool HasImage { get; set; }
        
        public bool RemoveImage { get; set; }

        public void LoadFromBlogPost(BlogPost post)
        {
            Id = post.Id;
            CreatedAt = post.CreatedAt;
            PostHeader = post.PostHeader;
            PostBody = post.PostBody;
            HasImage = post.HasImage; 
        }

        public async Task<BlogPost> LoadBlogPost(BlogPost post)
        {
            post.CreatedAt = CreatedAt;
            post.PostHeader = PostHeader;
            post.PostBody = PostBody;
            if (PostImage != null)
            {
                using (var memory = new MemoryStream())
                {
                    await PostImage.CopyToAsync(memory);
                    post.PostImage = memory.ToArray();
                }
                post.HasImage = true;
            }
            return post;
        }

        public async Task<BlogPost> EditBlogPost(BlogPost post)
        {
            post.PostHeader = PostHeader;
            post.PostBody = PostBody;
            post.Edited = true;
            post.LastEdited = DateTime.Now;
            if (RemoveImage)
            {
                post.PostImage = null;
                PostImage = null;
                post.HasImage = false;
                HasImage = false;
            }
            if (PostImage != null)
            {
                using (var memory = new MemoryStream())
                {
                    await PostImage.CopyToAsync(memory);
                    post.PostImage = memory.ToArray();
                }
                post.HasImage = true;
            }
            return post;
        }
    }
}
