using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TheSteveArmy.Services.CoolNeatServices.Sudoku;

namespace TheSteveArmy.Pages
{
    public class NewPuzzleModel : PageModel
    {
        public string Puzzle { get; set; }
        public void OnGet()
        {
            Puzzle = new NumberReducer().GeneratePuzzle(1);
        }
    }
}