using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TheSteveArmy.SudokuModels;
using TheSteveArmy.Services.CoolNeatServices.Sudoku;
namespace TheSteveArmy.Pages
{
    public class SudokuSolverModel : PageModel
    {
        [BindProperty]
        public SudokuModels.Row Row0 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row1 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row2 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row3 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row4 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row5 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row6 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row7 { get; set; }
        [BindProperty]
        public SudokuModels.Row Row8 { get; set; }
        public bool InvalidCharacters { get; set; }
        public bool ContradictingPuzzle { get; set; }
        public void OnGet()
        {
            Row0 = new SudokuModels.Row();
            Row1 = new SudokuModels.Row();
            Row2 = new SudokuModels.Row();
            Row3 = new SudokuModels.Row();
            Row4 = new SudokuModels.Row();
            Row5 = new SudokuModels.Row();
            Row6 = new SudokuModels.Row();
            Row7 = new SudokuModels.Row();
            Row8 = new SudokuModels.Row();


        }

        public IActionResult OnPost()
        {
            if (!CheckAllRowsForValidCharacters())
            {
                InvalidCharacters = true;
                return Page();
            }
            string puzzle = "";
            puzzle += GetRowContents(Row0);
            puzzle += GetRowContents(Row1);
            puzzle += GetRowContents(Row2);
            puzzle += GetRowContents(Row3);
            puzzle += GetRowContents(Row4);
            puzzle += GetRowContents(Row5);
            puzzle += GetRowContents(Row6);
            puzzle += GetRowContents(Row7);
            puzzle += GetRowContents(Row8);
            if (!SudokuBoard.IsCorrect(puzzle))
            {
                ContradictingPuzzle = true;
                return Page();
            }
            puzzle = SudokuBoard.Solve(puzzle);

            int counter = 0;
            //string puzzlePage = "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>";
            //puzzlePage += "<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js' integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN' crossorigin='anonymous'></script>";
            //puzzlePage += "<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js' integrity='sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q' crossorigin='anonymous'></script>";
            //puzzlePage += "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js' integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl' crossorigin='anonymous'></script>";
            string puzzlePage= "<style> td { font-size: 4em; border: 1px solid black; }</style>";
            puzzlePage+= "<h1 style='margin-left: 30%' class='display-1'>Steve Army Sudoku</h1>";
            puzzlePage+= "<br /><br /><br /><br />";
            puzzlePage+= "<table style='margin-left: 20%'>";
            for(int i = 0; i < 9; i++)
            {
                puzzle += "<tr>"; 
                for(int i2 = 0; i2 < 9; i2++)
                {
                    string num = puzzle[counter] + "";
                    counter++;
                    if (!num.Equals("0"))
                    {
                        if (counter % 2 == 0)
                        {
                            puzzlePage += "<td style='background-color: #E8E8E8'>";
                            puzzlePage += num + "&nbsp;&nbsp;&nbsp</td>";
                        }
                        else
                        {
                            puzzlePage += "<td>" + num + "&nbsp;&nbsp;&nbsp</td>";
                        }
                    }
                    else
                    {
                        if (counter % 2 == 0)
                        {
                            puzzlePage += "<td style='background-color: #E8E8E8'>";
                            puzzlePage += "&nbsp;&nbsp;&nbsp</td>";
                        }
                        else
                        {
                            puzzlePage += "<td>&nbsp;&nbsp;&nbsp</td>";
                        }
                    }
                }
                puzzlePage += "</tr>";
            }
            return Content(puzzlePage, "text/html");
        }

        public string GetRowContents(SudokuModels.Row row)
        {
            string rowContents = "";
            for (int i = 0; i < 9; i++)
            {
                if (String.IsNullOrEmpty(row.Cells[i].Number))
                {
                    rowContents += "0";
                }
                else
                {
                    rowContents += row.Cells[i].Number;
                }
            }
            return rowContents;
        }

        public bool CheckAllRowsForValidCharacters()
        {
            if (!RowHasValidCharacters(Row0)) return false;
            if (!RowHasValidCharacters(Row1)) return false;
            if (!RowHasValidCharacters(Row2)) return false;
            if (!RowHasValidCharacters(Row3)) return false;
            if (!RowHasValidCharacters(Row4)) return false;
            if (!RowHasValidCharacters(Row5)) return false;
            if (!RowHasValidCharacters(Row6)) return false;
            if (!RowHasValidCharacters(Row7)) return false;
            if (!RowHasValidCharacters(Row8)) return false;
            return true;
        }
        public bool RowHasValidCharacters(SudokuModels.Row row)
        {
            int character = 0;
            for (int i = 0; i < 9; i++)
            {
                if (String.IsNullOrEmpty(row.Cells[i].Number))
                {
                    continue;
                }
                character = -1;
                int.TryParse(row.Cells[i].Number, out character);
                if (character < 1 || character > 9)
                {
                    return false;
                }
            }
            return true;
        }
    }
}