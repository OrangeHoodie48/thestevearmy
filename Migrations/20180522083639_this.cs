﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TheSteveArmy.Migrations
{
    public partial class @this : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CreateAt",
                table: "BlogPosts",
                newName: "CreatedAt");

            migrationBuilder.AlterColumn<string>(
                name: "PostHeader",
                table: "BlogPosts",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "PostBody",
                table: "BlogPosts",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Edited",
                table: "BlogPosts",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Edited",
                table: "BlogPosts");

            migrationBuilder.RenameColumn(
                name: "CreatedAt",
                table: "BlogPosts",
                newName: "CreateAt");

            migrationBuilder.AlterColumn<string>(
                name: "PostHeader",
                table: "BlogPosts",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "PostBody",
                table: "BlogPosts",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
