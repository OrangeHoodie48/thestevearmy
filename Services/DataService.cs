﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Data;
using TheSteveArmy.Models;

namespace TheSteveArmy.Services
{
    public class DataService : IDataService
    {
        TheSteveArmyDbContext context;
        UserManager<IdentityModel> userManager;
        public DataService(TheSteveArmyDbContext context, UserManager<IdentityModel> userManager)
        {
            this.context = context;
            this.userManager = userManager; 
        }
        public BlogPost AddBlogPost(BlogPost post)
        {
            context.BlogPosts.Add(post);
            context.SaveChanges();
            return post;
        }
        public BlogPost RemoveBlogPost(int id)
        {
            BlogPost post = context.BlogPosts.FirstOrDefault(r => r.Id == id);
            if (post == null) return null;
            context.BlogPosts.Remove(post);
            context.SaveChanges();
            return post;
        }
        public BlogPost UpdateBlogPost(BlogPost post)
        {
            context.BlogPosts.Attach(post).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return post;
        }
        public BlogPost GetBlogPost(int id)
        {
            return context.BlogPosts.FirstOrDefault(r => r.Id == id);
        }
        public List<BlogPost> GetCurrentBlogPosts()
        {
            List<BlogPost> blogPosts = new List<BlogPost>();
            foreach (BlogPost post in context.BlogPosts)
            {
                if (post.Removed == false)
                {
                    blogPosts.Add(post);
                }
            }
            return blogPosts.OrderByDescending(r => r.CreatedAt.Ticks).ToList();
        }
        public Project AddProject(Project proj)
        {
            context.Projects.Add(proj);
            context.SaveChanges();
            return proj;
        }
        public Project RemoveProject(int id)
        {
            Project proj = context.Projects.FirstOrDefault(r => r.Id == id);
            context.Projects.Remove(proj);
            context.SaveChanges();
            return proj;
        }
        public Project UpdateProject(Project proj)
        {
            context.Projects.Attach(proj).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return proj;
        }
        public Project GetProject(int id)
        {
            return context.Projects.FirstOrDefault(r => r.Id == id);
        }
        public List<Project> GetProjects()
        {
            if(context.Projects.Count() > 0)
            {
                return context.Projects.ToList();
            }
            return new List<Project>();
        }
       public Comment AddComment(Comment comment)
        {
            context.Comments.Add(comment);
            context.SaveChanges();
            return comment;
        }
       public Comment RemoveComment(int id)
        {
            Comment comment = GetComment(id);
            context.Comments.Remove(comment);
            context.SaveChanges();
            return comment;
        }
       public Comment GetComment(int id)
        {
            return context.Comments.FirstOrDefault(r => r.Id == id);
        }
       public List<Comment> GetBlogPostComments(int id)
        {
            List<Comment> comments = new List<Comment>(); 
            foreach(Comment c in context.Comments)
            {
                if (c.PageId != id) continue; 

                if(c.PageType.Equals("Blog"))
                {
                    comments.Add(c);
                }
            }
            return comments;
        }
       public List<Comment> GetProjectComments(int id)
        {
            List<Comment> comments = new List<Comment>();
            foreach (Comment c in context.Comments)
            {
                if (c.PageId != id) continue;
                if (c.PageType.Equals("Project"))
                {
                    comments.Add(c);
                }
            }
            return comments;
        }
       public TempUser GetTempUser(int id)
        {
            return context.TempUsers.FirstOrDefault(r => r.Id == id);
        }
       public TempUser RemoveTempUser(int id)
        {
            TempUser user = GetTempUser(id);
            context.TempUsers.Remove(user);
            context.SaveChanges();
            return user;
        }
       public TempUser AddTempUser(TempUser user)
        {
            context.TempUsers.Add(user);
            context.SaveChanges();
            return user;
        }
        
        public bool TempUserTaken(string name, string pass)
        {
            DeleteOldTempUsers();
            foreach(TempUser temp in context.TempUsers)
            {
                if(temp.UserName.Equals(name) || temp.Password.Equals(pass))
                {
                    return true;
                }
            }
            return false;
        }
        
        public void DeleteOldTempUsers()
        {
            foreach(TempUser user in context.TempUsers)
            {
                if(user.CreatedAt.AddDays(1) < DateTime.Now)
                {
                    RemoveTempUser(user.Id);
                }
            }
        }

        public bool TempUserGoodForCompletingRegistration(TempUser user)
        {
            foreach(TempUser temp in context.TempUsers)
            {
                if(user.UserName.Equals(temp.UserName) && user.Password.Equals(temp.Password) && user.VerificationCode.Equals(temp.VerificationCode))
                {
                    return true;
                }   
            }
            return false;
        }
        public TempUser RemoveTempUserWithoutId(TempUser user)
        {
            foreach (TempUser temp in context.TempUsers)
            {
                if (user.UserName.Equals(temp.UserName) && user.Password.Equals(temp.Password) && user.VerificationCode.Equals(temp.VerificationCode))
                {
                    RemoveTempUser(temp.Id);
                    return user;
                }
            }
            return null;
        }
    }
}
