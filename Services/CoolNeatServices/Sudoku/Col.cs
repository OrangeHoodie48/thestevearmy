﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class Col : NumberContainer
    {
        public Col(SmartBoard board, int colNum): base()
        {
            ColNum = colNum;
            for (int i = 0; i < 9; i++)
            {
                Cells.Add(board.Board[i][colNum]);
            }
        }
        int ColNum; 
        internal override void InsertNumberIntoBoard(SmartBoard board)
        {
            for(int i = 0; i < 9; i++)
            {
                board.Board[i][ColNum].Number = Cells[i].Number;
            }
        }
        internal override void InsertCellsIntoBoard(SmartBoard board)
        {
            for (int i = 0; i < 9; i++)
            {
                board.Board[i][ColNum] = Cells[i];
            }
        }
    }
}
