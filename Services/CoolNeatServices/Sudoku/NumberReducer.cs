﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class NumberReducer
    {
        public Random random = new Random();
        public int InvocationsOnThisGrid = 0;

        internal string GeneratePuzzle(int difficultyLevel)
        {
            string newGrid = SudokuGenerator.Generate("Start");
            SmartBoard board = new SmartBoard(newGrid);
            Reduce(board, difficultyLevel);
            while(board.NumberOfEmptyCells() != 40)
            {
                InvocationsOnThisGrid = 0;
                newGrid = SudokuGenerator.Generate("Start");
                board = new SmartBoard(newGrid);
                Reduce(board, 1);
            }
            return board.ToString(); 
        }

        internal string Reduce(SmartBoard board, int difficultyLevel)
        {
            if(InvocationsOnThisGrid == 200)
            {
                return "Nope";
            }
            if (board.IsBroken == true)
            {
                return "Nope";
            }
            if (!board.IsCorrect())
            {
                return "Nope";

            }
            if (board.MeetsPuzzleRequirements == true)
            {
                return board.ToString();
            }
            string result = "";
            if(difficultyLevel == 1)
            {
                result = ReductionTester.EasyPuzzle(board); ;
            }
            if (!result.Equals("Continue"))
            {
                return result;
            }
            
            int randRow, randCol;
            int trueValue;
            Cell c;
            while (board.MeetsPuzzleRequirements == false)
            {
                if(InvocationsOnThisGrid > 200)
                {
                    break;
                }
                randRow = random.Next(9);
                randCol = random.Next(9);
                c = board.Board[randRow][randCol];
                trueValue = c.Number;
                c.Number = 0;
                if (Reduce(board, difficultyLevel).Equals("Nope"))
                {
                    InvocationsOnThisGrid++;
                    c.Number = trueValue;
                    board.IsBroken = false;
                }

            }
            return board.ToString();
        }
    }
}
