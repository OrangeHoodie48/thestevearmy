﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class Cell
    {
        public Cell()
        {
        }

        public Cell(int number, int row, int col, int boxRow, int boxCol): this()
        {
            Number = number;
            Row = row;
            Col = col;
            BoxRow = boxRow;
            BoxCol = boxCol;
        }

        public int Number { get; set; }
        public int Row { get; set; }
        public int Col { get; set; }
        public int BoxCol { get; set; }
        public int BoxRow { get; set; }



        public List<int> GetPossibleValues(SmartBoard board)
        {
            List<int> possibleValues = new List<int>(); 
            if(Number != 0)
            {
                return possibleValues; 
            }

            List<int> rowMissingNumbers = board.GetRow(Row).GetMissingNumbers();
            List<int> colMissingNumbers = board.GetCol(Col).GetMissingNumbers();
            List<int> boxMissingNumbers = board.GetBox(BoxRow, BoxCol).GetMissingNumbers();

            for(int i = 1; i < 10; i++)
            {
                if(rowMissingNumbers.Contains(i) && colMissingNumbers.Contains(i) && boxMissingNumbers.Contains(i))
                {
                    possibleValues.Add(i);
                }
            }

            return possibleValues; 
        }
    }
}
