﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class Box : NumberContainer
    {
        public Box(SmartBoard board, int boxRow, int boxCol): base()
        {

            BoxRow =boxRow;
            BoxCol = boxCol;

            int startCol = 3 * boxCol;
            int startRow = 3 * boxRow;
            for (int i = 0; i < 3; i++)
            {
                for(int i2 = 0; i2 < 3; i2++)
                {
                    Cells.Add(board.Board[i + startRow][i2 + startCol]);
                }

            }
        }
        int BoxRow;
        int BoxCol;

        internal override void InsertNumberIntoBoard(SmartBoard board)
        {
            int startRow = 3 * BoxRow;
            int startCol = 3 * BoxCol;
            int boxIndex = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int i2 = 0; i2 < 3; i2++)
                {
                    board.Board[i + startRow][i2 + startCol].Number = Cells[boxIndex].Number;
                    boxIndex++;
                }

            }
        }
        internal override void InsertCellsIntoBoard(SmartBoard board)
        {
            int startRow = 3 * BoxRow;
            int startCol = 3 * BoxCol;
            int boxIndex = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int i2 = 0; i2 < 3; i2++)
                {
                    board.Board[i + startRow][i2 + startCol] = Cells[boxIndex];
                    boxIndex++;
                }

            }

        }
    }
}
