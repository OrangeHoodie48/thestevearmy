﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class SudokuGenerator
    {
        private static Random random = new Random();
        public static string GenerateAndFormat()
        {
            string board = "Start";
            board = Generate(board);
            if (board.Equals("Nope"))
            {
                return board;
            }
            return FormatBoard(board);
        }
        public static string Generate(string board)
        {
            if (board.Equals("Start"))
            {
                board =
                        "000000000" +
                        "000000000" +
                        "000000000" +
                        "000000000" +
                        "000000000" +
                        "000000000" +
                        "000000000" +
                        "000000000" +
                        "000000000";
            }


            //First test for reported mistake and return if bad
            if (board.Equals("Nope"))
            {
                return board;
            }
            //See if there is a mistake and mark it and return
            //After this every option evaluated has no known mistakes as of yet.
            if (!IsCorrect(board))
            {
                return "Nope";
            }
            //The solved scenario.
            if (IsFull(board))
            {
                return board;
            }

            //To see the board every step of the way uncomment below
            //Console.WriteLine(FormatBoard(board));

            List<int> emptyIndices = new List<int>();
            List<int> missingNumbers = new List<int>();
            for (int rowNum = 0; rowNum < 9; rowNum++)
            {

                emptyIndices = GetEmptyCellIndices(board, rowNum * 9);
                missingNumbers = FindMissingNumbers(GetRow(board, rowNum));
                ShuffleList(missingNumbers);
                for (int i = 0; i < emptyIndices.Count; i++)
                {
                    for (int i2 = 0; i2 < missingNumbers.Count; i2++)
                    {

                        string possibleBoard = Generate(InsertValue(board, missingNumbers[i2]));
                        if (!possibleBoard.Equals("Nope"))
                        {
                            return possibleBoard;
                        }
                        else if (i2 == missingNumbers.Count - 1)
                        {
                            return "Nope";
                        }

                    }
                    return "Nope";
                }

            }

            return "Nope";

        }

        internal static string FormatBoard(string board)
        {
            string formattedBoard = "";
            string tempRow = "";
            string row = "";
            for (int i = 0; i < 9; i++)
            {
                tempRow = GetRow(board, i);
                for(int i2 = 0; i2 < tempRow.Length; i2++)
                {
                    row += tempRow[i2] + " "; 
                }
                formattedBoard += row + "\n";
                row = "";
            }

            return formattedBoard;
        }

        private static bool IsFull(string board)
        {
            string row = "";
            for (int i = 0; i < 9; i++)
            {
                row = GetRow(board, i);
                if (HasEmptyCells(row))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsCorrect(string board)
        {
            if (TestCols(board) == false)
            {
                return false;
            }
            if (TestRows(board) == false)
            {
                return false;
            }
            if (TestBoxes(board) == false)
            {
                return false;
            }

            return true;
        }

        private static bool TestCols(string board)
        {
            for (int i = 0; i < 9; i++)
            {
                if (ColIsCorrect(board, i) == false)
                {
                    return false;
                }
            }


            return true;
        }

        private static bool TestRows(string board)
        {
            for (int i = 0; i < 9; i++)
            {
                if (RowIsCorrect(board, i) == false) return false;
            }


            return true;
        }

        private static bool TestBoxes(string board)
        {

            for (int boxCol = 0; boxCol < 3; boxCol++)
            {
                for (int boxRow = 0; boxRow < 3; boxRow++)
                {
                    if (BoxIsCorrect(board, boxCol, boxRow) == false)
                    {
                        return false;
                    }

                }

            }

            return true;
        }

        private static bool ColIsCorrect(string board, int colNumber)
        {
            string col = GetCol(board, colNumber);
            if (SectionHasDoubles(col)) return false;
            return true;
        }

        private static bool RowIsCorrect(string board, int rowNumber)
        {
            string row = GetRow(board, rowNumber);
            if (SectionHasDoubles(row)) return false;

            return true;
        }

        private static string GetRow(string board, int rowNumber)
        {
            return board.Substring(rowNumber * 9, 9);
        }

        private static bool BoxIsCorrect(string board, int boxCol, int boxRow)
        {
            string box = GetBox(board, boxCol, boxRow);

            if (SectionHasDoubles(box)) return false;
            return true;
        }

        private static string GetBox(string board, int boxCol, int boxRow)
        {
            string box = "";
            int startPos = 3 * boxCol + 3 * (9 * boxRow);
            for (int i = 0; i < 3; i++)
            {
                box += board.Substring(startPos + (i * 9), 3);
            }

            return box;
        }

        private static bool HasDouble(string section, int number)
        {
            int count = 0;
            for (int i = 0; i < 9; i++)
            {

                if (int.Parse(section[i] + "") == number)
                {
                    count++;
                }

            }
            if (count > 1)
            {
                return true;
            }
            return false;
        }


        private static bool SectionHasDoubles(string section)
        {
            for (int i = 1; i < 10; i++)
            {
                if (HasDouble(section, i))
                {
                    return true;
                }
            }
            return false;
        }

        private static string GetCol(string board, int colNumber)
        {
            string col = "";
            for (int i = 0; i < 9; i++)
            {
                col += board.Substring(colNumber + (i * 9), 1);
            }
            return col;
        }

        private static bool HasEmptyCells(string section)
        {
            if (!section.Contains("0")) return false;
            return true;
        }


        private static List<int> FindMissingNumbers(string section)
        {
            List<int> missingNum = new List<int>();
            for (int i = 1; i < 10; i++)
            {
                if (!section.Contains(i + ""))
                {
                    missingNum.Add(i);
                }


            }
            return missingNum;
        }
        private static List<int> GetEmptyCellIndices(string board, int startPos)
        {
            List<int> indices = new List<int>();
            for (int i = startPos; i < startPos + 9; i++)
            {
                if (board[i] == '0')
                {
                    indices.Add(i);
                }
            }
            return indices;
        }

        private static string InsertValue(string board, int value)
        {
            int index = board.IndexOf("0");
            board = board.Insert(index, value + "");
            board = board.Remove(index + 1, 1);
            return board;
        }

        private static void ShuffleList(List<int> list)
        {
            if (list.Count < 2)
            {
                return;
            }
            List<int> indicesUsed = new List<int>();

            int length = list.Count;
            int index;
            int tempSpot;
            for (int i = 0; i < length; i++)
            {

                index = random.Next(length);
                while (indicesUsed.Contains(index))
                {
                    if (i == length - 1 && indicesUsed.Contains(i)) break;
                    index = random.Next(length);
                }
                indicesUsed.Add(index);

                tempSpot = list[i];
                list[i] = list[index];
                list[index] = tempSpot;
            }
        }
    }
}
