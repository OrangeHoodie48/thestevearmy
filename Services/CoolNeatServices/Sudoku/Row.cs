﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class Row : NumberContainer
    {
        public Row(SmartBoard board, int rowNum): base()
        {
            RowNum = rowNum; 
            for(int i = 0; i < 9; i++)
            {
                Cells.Add(board.Board[rowNum][i]);
            }
        }
        int RowNum;
        internal override void InsertNumberIntoBoard(SmartBoard board)
        {
            for (int i = 0; i < 9; i++)
            {
                board.Board[RowNum][i].Number = Cells[i].Number;
            }
        }
        internal override void InsertCellsIntoBoard(SmartBoard board)
        {
            for (int i = 0; i < 9; i++)
            {
                board.Board[RowNum][i] = Cells[i];
            }
        }
    }
}
