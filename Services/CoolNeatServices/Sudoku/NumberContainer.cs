﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    abstract class NumberContainer
    {
        public NumberContainer()
        {
            this.Cells = new List<Cell>();
        }
        internal List<Cell> Cells;

        public List<int> GetEmptyCellIndices()
        {
            List<int> emptyCellsIndices = new List<int>();
            for(int i = 0; i < 9; i++)
            {
                if(Cells[i].Number == 0)
                {
                    emptyCellsIndices.Add(i);
                }
            }
            return emptyCellsIndices; 
        }

        public bool HasEmptyCells()
        {
            foreach(Cell c in Cells)
            {
                if(c.Number == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public List<int> GetMissingNumbers()
        {
            List<int> missingNumbers = new List<int>();
            int[] array = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            missingNumbers.AddRange(array);

            for(int i = 0; i < 9; i++)
            {
                if (missingNumbers.Contains(Cells[i].Number))
                {
                    missingNumbers.Remove(Cells[i].Number);
                }
            }
            return missingNumbers; 
        }

        public override string ToString()
        {
            string s = "";
            foreach (Cell c in Cells)
            {
                s += c.Number;
            }
            return s;
        }

        internal virtual void InsertNumberIntoBoard(SmartBoard board)
        {
        }
        internal virtual void InsertCellsIntoBoard(SmartBoard board)
        {
        }
    }
}
