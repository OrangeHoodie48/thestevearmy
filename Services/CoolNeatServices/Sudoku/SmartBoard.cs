﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class SmartBoard
    {
        public SmartBoard(string board)
        {
            Board = new Cell[9][];
            Cell c;
            int boardCount = 0;
            int boxRow = 0, boxCol = 0;
            for (int i = 0; i <  9; i++)
            {
                this.Board[i] = new Cell[9];
                for(int i2 = 0; i2 <9; i2++)
                {
                    if(i < 3)
                    {
                        boxRow = 0;
                    }
                    else if(i < 6)
                    {
                        boxRow = 1; 
                    }
                    else
                    {
                        boxRow = 2;
                    }
                    
                    if(i2 < 3)
                    {
                        boxCol = 0; 
                    }
                    else if(i2 < 6)
                    {
                        boxCol = 1;
                    }
                    else
                    {
                        boxCol = 2;
                    }
                    c = new Cell(int.Parse(board[boardCount] + ""), i, i2, boxRow, boxCol);
                    this.Board[i][i2] = c;
                    boardCount++;
                }
            }
        }

        internal Cell[][] Board;
        internal bool IsBroken = false;
        internal bool MeetsPuzzleRequirements = false; 

        internal Row GetRow(int rowNum)
        {
            return new Row(this, rowNum);
        }
        internal Col GetCol(int colNum)
        {
            return new Col(this, colNum);
        }

        internal Box GetBox(int boxRow, int boxCol)
        {
            return new Box(this, boxRow, boxCol);
        }

        internal bool IsCorrect()
        {
            return SudokuBoard.IsCorrect(ToString());
        }

        internal bool HasEmptyCells()
        {
            Row row; 
            for(int i = 0; i < 9; i++)
            {
                row = GetRow(i);
                if (row.HasEmptyCells())
                {
                    return true;
                }
            }
            return false;
        }

        internal int NumberOfEmptyCells()
        {
            int num = 0;
            for(int i = 0; i < 9; i++)
            {
                num += GetRow(i).GetEmptyCellIndices().Count; 
            }
            return num;
        }

        public override string ToString()
        {
            string s = "";
            for(int i = 0; i < 9; i++)
            {
                for(int i2 = 0; i2 < 9; i2++)
                {
                    s += Board[i][i2].Number + ""; 
                }
            }
            return s; 
        }

        public string FormatBoard()
        {
            return SudokuGenerator.FormatBoard(ToString());
        }
    }
}
