﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    static class SmartBoardExtensionMethods
    {

        internal static int SolveOnlyPossibility(this SmartBoard board)
        {
            int numberOfCellsSolved = 0;
            for(int i = 0; i < 9; i++)
            {
                numberOfCellsSolved += EasyStrategies.SolveOnlyPossibility(board, board.GetRow(i));
                numberOfCellsSolved += EasyStrategies.SolveOnlyPossibility(board, board.GetCol(i));
                if(i < 3)
                {
                    for(int i2 = 0; i< 3; i++)
                    {
                        numberOfCellsSolved += EasyStrategies.SolveOnlyPossibility(board, board.GetBox(i, i2));
                    }
                }
            }
            return numberOfCellsSolved; 
        }
        internal static void SolveCrossExamination(this SmartBoard board)
        {
            Row row;
            for(int i = 0; i < 9; i++)
            {
                row = board.GetRow(i);
                for (int i2 = 0; i2 < 9; i2++)
                {
                    EasyStrategies.SolveCrossElimination(board, row.Cells[i2]);
                }
            }
        }

        internal static void SingleRowCrossExamination(this SmartBoard board, int rowNum)
        {
            Row row = board.GetRow(rowNum);
            for(int i = 0; i < 9; i++)
            {
                EasyStrategies.SolveCrossElimination(board, row.Cells[i]);
            }

        }

        //Recieves the board and cell coordinates and then attempts to find a value for that cell only
        //via cross examination. If the cell's value was unchanged from this invocation, the value 
        //false is returned and true otherwise.
        internal static bool SingleCellCrossExamination(this SmartBoard board, int rowNum, int colNum)
        {
            Cell c = board.Board[rowNum][colNum];
            if (c.Number != 0)
            {
                return false;
            }
            int originalNumber = c.Number;

            EasyStrategies.SolveCrossElimination(board, c);
            if(originalNumber != c.Number)
            {
                return true;
            }
            return false;
        }

    }
}
