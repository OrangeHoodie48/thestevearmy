﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class ReductionTester
    {
        
        internal static string EasyPuzzle(SmartBoard board)
        {
            SmartBoard board2 = new SmartBoard(board.ToString());
            int solvedByOnlyPossibility = 0;
            for (int i = 0; i < 6; i++)
            {
                solvedByOnlyPossibility += board2.SolveOnlyPossibility();
            }
            if (board.NumberOfEmptyCells() >= 6 && board.NumberOfEmptyCells() < board2.NumberOfEmptyCells() + 6)
            {
                board.IsBroken = true;
                return "Nope";
            }
            int numberOfEmptyCells;
            int tempSolvedByOnlyPossibility;
            int rowCounter = 0;
            int colCounter = 0;
            do
            {
                numberOfEmptyCells = board2.NumberOfEmptyCells();
                tempSolvedByOnlyPossibility = board2.SolveOnlyPossibility();
                solvedByOnlyPossibility += tempSolvedByOnlyPossibility;
                while (tempSolvedByOnlyPossibility != 0)
                {
                    tempSolvedByOnlyPossibility = board2.SolveOnlyPossibility();
                    solvedByOnlyPossibility += tempSolvedByOnlyPossibility;
                }

                while (board2.HasEmptyCells())
                {
                    if (board2.SingleCellCrossExamination(rowCounter, colCounter))
                    {
                        rowCounter = 0;
                        colCounter = 0;
                        break;
                    }
                    else if (rowCounter < 8 && colCounter == 8)
                    {
                        rowCounter++;
                        colCounter = 0;
                    }
                    else if (colCounter < 8)
                    {
                        colCounter++;
                    }
                    else if (rowCounter == 8 && colCounter == 8)
                    {
                        break;
                    }
                }
            } while (numberOfEmptyCells != board2.NumberOfEmptyCells());

            if (board2.HasEmptyCells())
            {
                board.IsBroken = true;
                return "Nope";
            }
            if(40 - board.NumberOfEmptyCells() < 15 - solvedByOnlyPossibility)
            {
                board.IsBroken = true;
                return "Nope";
            }
            if (board.NumberOfEmptyCells() == 40)
            {
                board.MeetsPuzzleRequirements = true;
                return board.ToString();
            }

            return "Continue";
        }
    }
}
