﻿using System;
using System.Collections.Generic;
using System.Text;
namespace TheSteveArmy.Services.CoolNeatServices.Sudoku
{
    class EasyStrategies
    {
        public static int SolveOnlyPossibility(SmartBoard board, NumberContainer cont)
        {

            List<int> missingNumbers = cont.GetMissingNumbers(); 
            if(missingNumbers.Count != 1)
            {
                return 0; 
            }
            cont.Cells[cont.GetEmptyCellIndices()[0]].Number = missingNumbers[0];
            return 1; 
        }

        public static void SolveCrossElimination(SmartBoard board, Cell c)
        {
            if (c.Number != 0) return;
            List<int> possibleValues = c.GetPossibleValues(board);

            if (possibleValues.Count == 1)
            {
                c.Number = possibleValues[0];
                return;
            }
            
            Row row = board.GetRow(c.Row);
            Col col = board.GetCol(c.Col);
            Box box = board.GetBox(c.BoxRow, c.BoxCol);

            CrossExamineCheckContainerHelperMethod(board, c, possibleValues, row);
            if (c.Number == 0) CrossExamineCheckContainerHelperMethod(board, c, possibleValues, col);
            if (c.Number == 0) CrossExamineCheckContainerHelperMethod(board, c, possibleValues, box);

        }

        private static void CrossExamineCheckContainerHelperMethod(SmartBoard board, Cell c, List<int> possibleValues, NumberContainer cont)
        {
            List<int> emptyIndices = cont.GetEmptyCellIndices();
            List<int> valuesLeft; valuesLeft = c.GetPossibleValues(board);
            Cell c2;
            foreach (int i in emptyIndices)
            {
                c2 = cont.Cells[i];
                if (c2 == c)
                {
                    continue;
                }

                List<int> c2PossibleValues = c2PossibleValues = c2.GetPossibleValues(board);
                foreach (int i2 in possibleValues)
                {
                    if (c2PossibleValues.Contains(i2))
                    {
                        valuesLeft.Remove(i2);
                    }

                    if (valuesLeft.Count == 0) break;

                }

                if (valuesLeft.Count == 0) break;

            }

            if (valuesLeft.Count == 1)
            {
                c.Number = valuesLeft[0];
            }

        }
    }
}
