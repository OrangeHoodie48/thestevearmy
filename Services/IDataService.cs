﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;

namespace TheSteveArmy.Services
{
    public interface IDataService
    {
        BlogPost AddBlogPost(BlogPost post);
        BlogPost RemoveBlogPost(int id);
        BlogPost UpdateBlogPost(BlogPost post);
        BlogPost GetBlogPost(int id);
        List<BlogPost> GetCurrentBlogPosts();
        Project AddProject(Project proj);
        Project RemoveProject(int id);
        Project UpdateProject(Project proj);
        Project GetProject(int id);
        List<Project> GetProjects();
        Comment AddComment(Comment comment);
        Comment RemoveComment(int id);
        Comment GetComment(int id);
        List<Comment> GetBlogPostComments(int id);
        List<Comment> GetProjectComments(int id);
        TempUser GetTempUser(int id);
        TempUser RemoveTempUser(int id);
        TempUser AddTempUser(TempUser user);
        bool TempUserTaken(string name, string pass);
        void DeleteOldTempUsers();
        bool TempUserGoodForCompletingRegistration(TempUser user);
        TempUser RemoveTempUserWithoutId(TempUser user);
    }
}
