﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TheSteveArmy.Data;
using TheSteveArmy.Models;
using TheSteveArmy.Services;

namespace TheSteveArmy
{
    public class Startup
    {
        IConfiguration configuration;
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<IdentityModel, IdentityRole>(config => {
                config.Password.RequiredLength = 8;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireDigit = true;
                //Uncomment the following statement when publishing. 
                config.User.RequireUniqueEmail = true;
            }).AddEntityFrameworkStores<TheSteveArmyDbContext>();
            services.AddMvc();
            services.AddScoped<IDataService, DataService>();
            services.AddDbContext<TheSteveArmyDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("TheSteveArmy")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication(); 
            app.UseMvc(BuildRoutes);


            app.UseRewriter(new RewriteOptions().AddRedirectToHttpsPermanent());
            app.Run(async (context) =>
            {
                string html = "<style>div {text-align: center;}</style>" +
                "<div><h1>The Steve Army Is Coming soon!</h1><h2>Victory Is Awaiting Us Steves!</h2></div>";
                await context.Response.WriteAsync(html);
            });
        }

        public void BuildRoutes(IRouteBuilder builder)
        {
            builder.MapRoute("Default", "{controller=Home}/{action=Index}/{id?}");
        }
    }
}
