﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Data;

namespace TheSteveArmy
{
    public class TheSteveArmyDbContextFactory : IDesignTimeDbContextFactory<TheSteveArmyDbContext>
    {
        public TheSteveArmyDbContextFactory()
        {

        }

        IConfigurationRoot config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

        public TheSteveArmyDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TheSteveArmyDbContext>();
            optionsBuilder.UseSqlServer(config.GetConnectionString("TheSteveArmy"));
            return new TheSteveArmyDbContext(optionsBuilder.Options);
        }
    }
}
