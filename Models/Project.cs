﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheSteveArmy.Models
{
    public class Project
    {
        public int Id { get; set; }
        [Required]
        public string Heading { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public byte[] Image { get; set; }
        [Required]
        public ProjectCategoriesEnum Category { get; set; }
        [Required]
        public string BriefDescription {get; set;}
        public string Repository { get; set; }
        public string AdditionalLinks { get; set; }
    }
}
