﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TheSteveArmy.Models
{
    public class BlogPostWrapper
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string PostHeader { get; set; }
        public string PostBody { get; set; }

        public bool Removed { get; set; }
        public bool Edited { get; set; }
        public DateTime LastEdited { get; set; }

        public byte[] PostImage { get; set; }

        public bool HasImage { get; set; }

        public List<Comment> Comments { get; set; }
        public string NewComment { get; set; }
        public string NewCommentUserName { get; set; }

        public void LoadFromBlogPost(BlogPost post)
        {
            Id = post.Id;
            CreatedAt = post.CreatedAt;
            PostHeader = post.PostHeader;
            PostBody = post.PostBody;
            Removed = post.Removed;
            Edited = post.Edited;
            LastEdited = post.LastEdited;
            PostImage = post.PostImage;
            HasImage = post.HasImage;
        }
    }
}
