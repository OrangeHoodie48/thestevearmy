﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheSteveArmy.Models
{
    public class BlogPost
    {
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        [Required]
        public string PostHeader { get; set; }
        [Required]
        public string PostBody { get; set; }
        
        public bool Removed { get; set; }
        public bool Edited { get; set; }
        public DateTime LastEdited { get; set; }

        public byte[] PostImage { get; set; }

        public bool HasImage { get; set; }

        public List<Comment> Comments { get; set; }
    }
}
