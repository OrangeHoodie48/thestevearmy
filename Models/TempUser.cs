﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TheSteveArmy.Models
{
    public class TempUser
    {
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public int VerificationCode { get; set; }
        public DateTime CreatedAt { get; set; }
        [Required]
        public string Email { get; set; }
        public void SetVerificationCode()
        {
            Random rand = new Random();
            VerificationCode = rand.Next(1000000) + 1000000;
        }
        public string BuildEmail()
        {
            SetVerificationCode();
            string message = "To create your account, visit the following url and sign in with your username and " +
                             " password and the following vertification code: " + VerificationCode + "\n";
            message += "https://thestevearmy20180514120803.azurewebsites.net/Account/CompleteRegistration";
            return message; 
        }
    }
}
