﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TheSteveArmy.Models;

namespace TheSteveArmy.Data
{
    public class TheSteveArmyDbContext : IdentityDbContext<IdentityModel>
    {
        public TheSteveArmyDbContext(DbContextOptions options): base(options)
        {
        }

        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<IdentityModel> IdentityModels { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<TempUser> TempUsers { get; set; }
    }
}
